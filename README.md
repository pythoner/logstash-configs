# About #

This repo is a collection of logstash configurations files. The parsers (filters) have been written by various authors online and respective credits have been provided in each configuration. 

### Disclaimer ###

* The configurations are provided as-is. These are provided for learning purpose and should be validated and verified before deploying in producting. If you do notice a bug in any parser, please free to submit a bug report.

### Contribution guidelines ###

* If you have any custom parser which you don't mind sharing, please create a pull request or drop me a message at the below coordinates

### Contact ###

* Wasim Halani 
* Email: wasim.halani [@] niiconsulting.com / wasimhalani [@] gmail.com
* Twitter: [@washalsec](https://twitter.com/washalsec)
* LinkedIn: [https://www.linkedin.com/in/wasimhalani/](https://www.linkedin.com/in/wasimhalani/)